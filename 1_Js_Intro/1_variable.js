// Basics of var in JS

// variable declaration - var, let, const

// var

//var a // variable will be initializes as undefined
//console.log(a)

// JS is a dynamically typed language - datatype is not specified

// datatypes in JS - number, String, boolean, undefined

//var b = 2.3 // as a number
//var c = 'd'         // string
//var d = 'ADsfps'    // string
//var e = true        // boolean
//var f               // undefined

// Var has some problems

// 1. Redeclaration
//var b = 'hello'
//
//var b = 'bye'
//
//console.log(b)

// To overcome redclaration

// Use let keyword

// let b = 'hello'
// console.log(b)
// let b = 'bye' // redeclaration is not allowed
// 
// console.log(b)
// 
// 
// let b = 'hello'
// console.log(b)
// b = 'bye' // reassigning is allowed

//console.log(b)

// loops

// for loop

// Prime number

//var flag = true
//
//var num = 13
//
//for (var i=2; i*i <= num; i++)
//{
//    if (num%i==0)
//    {
//        flag = false
//        break;
//    }
//}
//
//if (flag==true)
//{
//    console.log('Prime')
//}
//else{
//  console.log("Not prime")
//}

// 2nd problem with var - SCOPING

//if (10%2==0)
//{
//    var a = 2   // scoped variable- should be accessible only in this block
//    console.log(a)
//}
//
//console.log(a) // can be accessed outside if block


// const

const a = 2
console.log(a)

a = 12 // reassignment and redeclaration is not allowed

