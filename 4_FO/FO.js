const  help = require('./commands/help')
const organize = require('./commands/organize')
const tree = require('./commands/tree')

const { dir } = require('console');
const fs = require('fs');
const { type } = require('os');
const path = require('path');

let input = process.argv.slice(2);

let command = input[0]

switch(command)
{
    case 'tree':
        tree.treeKey(input[1])
        break;
    case 'organize':
        organize.organizeKey(input[1])
        break;
    case 'help':
        help.helpKey();
        break;

    default:
        console.log('Please enter a valid command')
        break;
}
