const { dir } = require('console');
const fs = require('fs');
const { type } = require('os');
const path = require('path');

function treeFn(dirPath)
{
    if (dirPath==undefined)
    {
        console.log("Please enter a valid command.")
        return;
    }
    else {
        let doesExist = fs.existsSync(dirPath);

        if (doesExist)
        {
            treeHelper(dirPath, " ")
        }
        else{
            console.log("Please enter a valid path.")
        }

    }

    
}

function treeHelper(targetPath, indent)
{
    let isFile = fs.lstatSync(targetPath).isFile()

    if(isFile)
    {
        let fileName = path.basename(targetPath)
        console.log(indent + "├──" + fileName)
    }
    else
    {
        let dirName = path.basename(targetPath)
        console.log(indent + '└──' + dirName)
        //indent = indent+" "
        
        let children = fs.readdirSync(targetPath)
        for (let i=0; i<children.length; i++)
            { 
                treeHelper(path.join(targetPath,children[i]), indent + '\t')
            }
    }



}



module.exports = 
{
    treeKey : treeFn
}