const fs = require('fs')

const path = require('path')
// 1 Reading
/* 
let content = fs.readFileSync('f1.txt')

console.log("Content->"+content)

// 2 Writing

st = "Hello World"

fs.writeFileSync("f2.txt",st)
console.log("Content->"+content)

// 3 Updating

fs.appendFileSync("f2.txt"," This is new Data")

// delete a file

fs.unlinkSync('f2.txt')
console.log('F2 Deleted') */


// Directories

// Creating
// mkdirSync method is used to create new Directory

/* fs.mkdirSync('myDir')
 */
// Deleting

/* fs.rmdirSync('myDir') */


// To check whether dir exists

/* let doesExist = fs.existsSync('myDir')

console.log(doesExist)
 */

// Stats of directory

/* let stats = fs.lstatSync('myDir') */
/* console.log(stats) */

/* console.log('isFile?', stats.isFile()) */


// readDirSync

/* let folderpath = 'C:\\Users\\arjun\\Desktop\\Dev\\3_Node_js\\myDir'

let foldercontent = fs.readdirSync(folderpath)

console.log("Dir content- " + foldercontent) */

// Copying Files

// src file, dest file

let srcFilePath = 'C:\\Users\\arjun\\Desktop\\Dev\\3_Node_js\\myDir\\1'

let destinationPath = 'C:\\Users\\arjun\\Desktop\\Dev\\3_Node_js\\myDir2'

let toBeCopiedFileName = path.basename(srcFilePath)

let destPath = path.join(destinationPath, toBeCopiedFileName)
console.log(destPath)

fs.copyFileSync(srcFilePath,destPath)