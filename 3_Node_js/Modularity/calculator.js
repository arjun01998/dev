function add (a,b){
    console.log("Sum: ",a+b)
}
function mul (a,b){
    console.log("Product: ",a*b)
}
function div (a,b){
    console.log("Quotient: ",a/b)
}
function sub (a,b){
    console.log("Difference: ",a-b)
}


module.exports={
    addition: add,
    subtract: sub,
    multiply: mul,
    division: div,
}